package GenAva.GenAva;

import org.apache.commons.codec.digest.DigestUtils;

class Name{
	private String name;
	private String hashname;
	
	Name(String name){
		this.name = name;
		this.hashname = DigestUtils.md5Hex(this.name);
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getHashName() {
		return this.hashname;
	}
}