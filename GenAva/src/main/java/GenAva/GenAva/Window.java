package GenAva.GenAva;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Window {
	
	public static void getScreen() throws Exception {
	    Rectangle screenRect = new Rectangle(820, 410, 270, 250); // Размеры скрина. Видимо, придётся поменять на какие-то автоматические, но я не знаю, как это сделать.
	    BufferedImage capture = new Robot().createScreenCapture(screenRect);

	    File imageFile = new File("I:\\I131\\Java\\eclipse\\GenAva\\GenAva\\ava.bmp");
	    ImageIO.write(capture, "bmp", imageFile );
	    //assertTrue(imageFile .exists());
	}

	public static void main(String args[]) throws Exception {
		
		JFrame window = new JFrame("Генератор аватарки");	 // Задание заголовка окна
		
		window.setSize(300, 300);							// Задание размеров окна
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Закрытие при нажатии на крестик
		window.setLayout(new BorderLayout(1,1)); // Каким образом в окне расположены объекты
		window.setLocationRelativeTo(null);		// Чтобы окно всегда было посередине экрана
		
		Ava ava = new Ava();
		window.add(ava);
		window.setVisible(true);
		
		Thread.sleep(2000);
		
		getScreen();
	}	
}
