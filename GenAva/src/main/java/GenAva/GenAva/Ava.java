package GenAva.GenAva;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;

//Класс, который будет рисовать элементы
public class Ava extends JComponent{
	
	// Метод, перерисовывающий элемент внутри окна при обновлении
	@Override
	protected void paintComponent(Graphics graphics){
		super.paintComponents(graphics);
		
		// Матрица 12*6
		final int SIZE_H = 12;
		final int SIZE_W = 6;
		
		boolean [][] matrix  = new boolean [SIZE_H][SIZE_W]; // Половинчатая матрица
		
		// Рандомный цвет
		final int MIN = 1;
		final int MAX = 7;
		
		int c = (int)(Math.random()*((MAX-MIN)+1))+MIN;
				
		switch(c) {
		case 1: graphics.setColor(Color.RED); break;
		case 2: graphics.setColor(Color.ORANGE); break;
		case 3: graphics.setColor(Color.YELLOW); break;
		case 4: graphics.setColor(Color.GREEN); break;
		case 5: graphics.setColor(Color.BLUE); break;
		case 6: graphics.setColor(Color.CYAN); break;
		case 7: graphics.setColor(Color.MAGENTA); break;
		
		default: graphics.setColor(Color.RED);
		}
		
		Name name = new Name("AgentMoroz"); // Как-то получаем имя
		
		for (int i = 0; i < SIZE_H; i++) {
			for (int j = 0; j < SIZE_W; j++){
				char ch = name.getHashName().charAt(i + j);
				if (ch < '0' || ch > '9') {
					matrix[i][j] = true;
				}
			}
		}
		
		boolean [][] fullmatrix = new boolean [SIZE_H][SIZE_H]; // Полная, окончательная матрица
		
		// Пытаемся отзеркаливать
		for (int i = 0; i < SIZE_H; i++) {
			for (int j = 0; j < SIZE_W; j++) {
				fullmatrix[i][j] = matrix[i][j];
				fullmatrix[i][SIZE_H - j - 1] = matrix[i][j];
			}
		}
		
		// Рисуем по полной матрице
		for (int i = 0; i < SIZE_H; i++) {
			for (int j = 0; j < SIZE_H; j++){
				if (fullmatrix[i][j]) {
					graphics.fillRect(10 + j*20, 10 + i*20, 20, 20);
				}
			}
		}
		
		// Рамка из пустых блоков
		graphics.setColor(getBackground());
		for (int i = 0; i < SIZE_H; i++) {
			graphics.fillRect(10 + 20*i, 10, 20, 20);
			graphics.fillRect(10, 10 + 20*i, 20, 20);
			graphics.fillRect(10 + 220, 10 + 20*i, 20, 20);
			graphics.fillRect(10 + 20*i, 10 + 220, 20, 20);
		}
		
		//super.repaint();
	}
}
